# GitLab CI/CD Example

- Blocks pushing directly to `master`. Must submit merge request (MR) instead.
- Auto-runs pipeline on MRs. Merge is blocked if either:
  - Any ESLint errors are found.
  - Any tests fail.
